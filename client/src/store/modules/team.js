import { get } from "../../service/api";
export default {
  namespaced: true,
  state: {
    showOneTeam: true,
    teams: [],
    statOptions: [],
    payOptions: [],
    deleteFromTeamEvents: [],
    conditionOptionResult: [],
    activeTeam: {
      id: "",
      link: "",
      linkExpired: "",
      am_i_teamlid: false,
      name: "",
      status: "",
      img: "",
      options: {
        statOption: 1,
        payOption: 1
      },
      masters: []
    }
  },

  mutations: {
    SET_ACTIVE_TEAM_MASTERS(state, MASTERS) {
      state.activeTeam.masters = MASTERS;
    },
    SET_ACTIVE_TEAM(state, team) {
      state.activeTeam = team;
    },
    SET_TEAM_LIST(state, teams) {
      state.teams = teams;
    }
  },
  actions: {
    editTeam(ctx, teamObject) {
      // route to server for update team, then commit changes!
      ctx.commit("SET_ACTIVE_TEAM", teamObject);
    },
    getMyTeams(ctx, limit) {
      return get(`teams?limit=${limit}`).then(res => {
        ctx.commit("SET_TEAM_LIST", res.data);
        ctx.commit("SET_ACTIVE_TEAM", res.data[0]);

        return res;
      });
    },
    async getDict(ctx) {
      const dict = await get("team/options");
      ctx.state.statOptions = dict.data.statOptions;
      ctx.state.payOptions = dict.data.payOptions;
      ctx.state.deleteFromTeamEvents = dict.data.deleteFromTeamEvents;
      ctx.state.conditionOptionResult = dict.data.conditionOptionResult;

      return dict.data;
      // .then(res => {

      // });
    }
  },
  getters: {
    masterColumns(state) {
      return {
        1: {
          class: "stat-var-1",
          name: {
            active: true
          },
          "confirm-lid": {
            active: true
          },
          "tested-offers": {
            active: true
          },
          "summ-work": {
            active: true
          },
          change: {
            active: true
          },
          balance: {
            active: true
          },
          status: {
            active: true
          }
        },
        2: {
          class: "stat-var-2",
          name: {
            active: true
          },
          "confirm-lid": {
            active: true
          },
          "tested-offers": {
            active: true
          },
          "summ-work": {
            active: true
          },
          change: {
            active: true
          },
          balance: {
            active: true
          },
          status: {
            active: true
          }
        },
        3: {
          class: "stat-var-3",
          name: {
            active: true
          },
          "confirm-lid": {
            active: state.activeTeam.am_i_teamlid ? true : false
          },
          "tested-offers": {
            active: state.activeTeam.am_i_teamlid ? true : false
          },
          "summ-work": {
            active: true
          },
          change: {
            active: true
          },
          balance: {
            active: state.activeTeam.am_i_teamlid ? true : false
          },
          status: {
            active: state.activeTeam.am_i_teamlid ? true : false
          }
        }
      };
    },
    teamOptions(state) {
      return state.statOptions.length
        ? [
            {
              text: state.statOptions.find(o => o.value == state.activeTeam.options.statOption)
                .text,
              value: state.statOptions.find(o => o.value == state.activeTeam.options.statOption)
                .value,
              success: true
            },
            {
              text: state.payOptions.find(o => o.value == state.activeTeam.options.payOption).text,
              value: state.payOptions.find(o => o.value == state.activeTeam.options.payOption)
                .value,
              success: true
            }
          ]
        : [];
    }
  }
};

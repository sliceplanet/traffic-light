import api from './axios';

function baseHost() {
    return 'http://194.58.108.17:8588'
}

function get(url) {
    return api().get(url)
}

function post(url, data) {
    return api().post(url, data);
}

export { get, post, baseHost };

import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store/store";
import VueClipboard from 'vue-clipboard2'

// hack for bootstrap modals
VueClipboard.config.autoSetContainer = true // add this line

import {
  NavbarPlugin,
  TooltipPlugin,
  FormPlugin,
  FormInputPlugin,
  LayoutPlugin
} from "bootstrap-vue";

Vue.use(LayoutPlugin);
Vue.use(FormInputPlugin);
Vue.use(FormPlugin);
Vue.use(TooltipPlugin);
Vue.use(VueClipboard)


import "./styles/styles.scss";

Vue.config.productionTip = false;
Vue.use(NavbarPlugin);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");

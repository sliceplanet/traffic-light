var express = require("express");
var router = express.Router();

router.get("/notifications/list", (req, res) => {
  res.send({
    count_all: 1,
    data: [
      {
        object: "dashboard", // "Dashbord",
        count: 1
      }
    ],
    error: 0 // если все ок - 0
  });
});

router.get("/user/get", (req, res) => {
  res.send({
    purse: {
      balance: {
        currency: {
          rub: 0,
          dollar: 0,
          eur: 0
        },
        bonus: 50
      },
      hold: {
        rub: 50000,
        dollar: 50,
        eur: 50
      },
      expect: {
        rub: 50,
        dollar: 50,
        eur: 50
      }
    },
    rate: 63.91,
    error: 0
  });
});

router.post("/operating/list", (req, res) => {
  res.send({
    leads: {
      count: 128,
      state: "up"
    },
    ranking: {
      site: 12,
      state: "up" // up, down, without_change
    },
    income: {
      currency: 25000,
      bonus: 120
    },
    data: {
      current: [
        {
          value: 500.5,
          time: "2019-10-16T10:00:00+03:00"
        },
        {
          value: 200,
          time: "2019-10-16T11:00:00+03:00"
        },
        {
          value: 700,
          time: "2019-10-16T12:00:00+03:00"
        },
        {
          value: 800,
          time: "2019-10-16T13:00:00+03:00"
        },
        {
          value: 200,
          time: "2019-10-16T14:00:00+03:00"
        }
      ],
      old: req.body.previous
        ? [
            {
              value: 500,
              time: "2019-10-16T10:00:00+03:00"
            },
            {
              value: 400,
              time: "2019-10-16T11:00:00+03:00"
            },
            {
              value: 1000,
              time: "2019-10-16T12:00:00+03:00"
            },
            {
              value: 2000,
              time: "2019-10-16T13:00:00+03:00"
            },
            {
              value: 400,
              time: "2019-10-16T14:00:00+03:00"
            }
          ]
        : []
    },
    error: 0
  });
});

router.get("/achievements/list", (req, res) => {
  res.send({
    count_all: 9,
    data: [
      {
        title: "Протестировать 3 оффера",
        prompt:
          "Оффер считается протестированным, если по нему имеется хотя бы 1 подтвержденный лид. Счетчик обнуляется 1 числа каждого месяца. Офферы, по которым имеются лиды в прошлых месяцах, не учитываются. Достижение будет получено, когда вы наберете 3 протестированных оффера за текущий месяц.",
        reward: 100,
        stage_current: 3,
        stage_all: 3
      },
      {
        title: "Протестировать 6 офферов",
        prompt:
          "Оффер считается протестированным, если по нему имеется хотя бы 1 подтвержденный лид. Счетчик обнуляется 1 числа каждого месяца. Офферы, по которым имеются лиды в прошлых месяцах, не учитываются. Достижение будет получено, когда вы наберете 6 протестированных офферов за текущий месяц.",
        reward: 500,
        stage_current: 1,
        stage_all: 6
      },
      {
        title: "Протестировать 9 офферов",
        prompt:
          "Оффер считается протестированным, если по нему имеется хотя бы 1 подтвержденный лид. Счетчик обнуляется 1 числа каждого месяца. Офферы, по которым имеются лиды в прошлых месяцах, не учитываются. Достижение будет получено, когда вы наберете 9 протестированных офферов за текущий месяц.",
        reward: 1000,
        stage_current: 1,
        stage_all: 9
      },
      {
        title: "Не останавливать трафик 5+ дней подряд",
        prompt:
          "Не останавливай трафик 5 дней подряд и начнешь получать больше лайтов за каждый лид. Этот коэффициент будет прибавлять по 0,1 с каждым днем пролива, достигнув максимума на 14-ый день: 2,0. В случае, если трафик будет остановлен на сутки (за сутки не будет зафиксировано ни одного подтвержденного лида), отсчет обнулится, и коэффициент вернется к 1,0. Внимание: учитываются только подтвержденные лиды. Если на протяжении суток вы не получили ни одного аппрува, коэффициент вернется к 1,0 даже при наличии сырых лидов.",
        reward: 2000,
        stage_current: 2,
        stage_all: 5
      },
      {
        title: "Получить 100 подтвержденных лидов",
        prompt: null,
        reward: 250,
        stage_current: 0,
        stage_all: 100
      },
      {
        title: "Получить 1000 подтвержденных лидов",
        prompt: null,
        reward: 2000,
        stage_current: 0,
        stage_all: 1000
      },
      {
        title: "Оставить отзыв на Partnerkin.com",
        prompt:
          "Напиши отзыв о Traffic Light на Partnerkin.com и отправь скрин отзыва в чат поддержки.",
        reward: 120,
        stage_current: 0,
        stage_all: 1
      },
      {
        title: "Оставить отзыв на Cpainform.ru",
        prompt:
          "Напиши отзыв о Traffic Light на Cpainform.ru и отправь скрин отзыва в чат поддержки.",
        reward: 60,
        stage_current: 0,
        stage_all: 1
      },
      {
        title: "Оставить отзыв на Conversion.im",
        prompt:
          "Напиши отзыв о Traffic Light на Conversion.im и отправь скрин отзыва в чат поддержки.",
        reward: 60,
        stage_current: 0,
        stage_all: 1
      }
    ],
    error: 0
  });
});

router.get("/offer/:offerId/access", (req, res) => {
  res.send({
    tasks: [
      {
        text: "лендинг",
        value: true
      },
      {
        text: "лендинг2",
        value: true
      },
      {
        text: "лендинг3",
        value: true
      },
      {
        text: "лендинг4",
        value: false
      },
      {
        text: "лендинг6",
        value: false
      },
      {
        text: "лендинг - exit",
        value: false
      },
      {
        text: "лендинг Длинное название",
        value: false
      }
    ],
    masters: [
      {
        id: "873948573jsdljfl",
        value: true,
        openDate: "23.11.2019"
      }
    ]
  });
});

router.get("/offers/list", (req, res) => {
  res.send({
    count_all: 9,
    data: [
      {
        img: "img/slider_image_1.jpg",
        title: "Телевизионная антенна HQClearTv",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_3.jpg",
        title: "Perfect Smile Veneers",
        link: "#",
        time: "26.04.2019",
        type: "private",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        time: "26.04.2019",
        type: null,
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          }
        ]
      },
      {
        img: "img/slider_image_1.jpg",
        title: "Телевизионная антенна HQClearTv",
        link: "#",
        time: "26.04.2019",
        type: null,
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          }
        ]
      },
      {
        img: "img/slider_image_1.jpg",
        title: "Телевизионная антенна HQClearTv",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      }
    ],
    error: 0
  });
});
router.get("/offers/private/list", (req, res) => {
  res.send({
    count_all: 9,
    data: [
      {
        img: "img/slider_image_1.jpg",
        title: "Телевизионная антенна HQClearTv",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: true,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_3.jpg",
        title: "Perfect Smile Veneers",
        link: "#",
        time: "26.04.2019",
        type: "private",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        time: "26.04.2019",
        type: null,

        hasAccess: true,
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          }
        ]
      },
      {
        img: "img/slider_image_1.jpg",
        title: "Телевизионная антенна HQClearTv",
        link: "#",
        time: "26.04.2019",
        type: null,

        hasAccess: false,
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          }
        ]
      },
      {
        img: "img/slider_image_1.jpg",
        title: "Телевизионная антенна HQClearTv",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          },
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 600
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      },
      {
        img: "img/slider_image_2.jpg",
        title: "Домашние грибницы за 147р",
        link: "#",
        type: null,

        hasAccess: false,
        time: "26.04.2019",
        price: [
          {
            country: "RUS",
            price: 1290,
            affiliate_payout: 400
          },
          {
            country: "UA",
            price: 1290,
            affiliate_payout: 500
          }
        ]
      }
    ],
    error: 0
  });
});

router.get("/regalia/list", (req, res) => {
  res.send({
    data: [
      {
        title: "tester",
        value: 0
      },
      {
        title: "exporter",
        value: 5
      },
      {
        title: "lead_machine",
        value: 2
      },
      {
        title: "beauty",
        value: 5
      },
      {
        title: "accessory_supplier",
        value: 5
      },
      {
        title: "health",
        value: 2
      },
      {
        title: "jobs",
        value: 0
      },
      {
        title: "fish",
        value: 2
      },
      {
        title: "gamer",
        value: 0
      },
      {
        title: "professor",
        value: 3
      },
      {
        title: "approve",
        value: 2
      },
      {
        title: "business_executive",
        value: 0
      },
      {
        title: "adult",
        value: 3
      },
      {
        title: "fashion",
        value: 2
      },
      {
        title: "tuning",
        value: 1
      },
      {
        title: "esoteric",
        value: 0
      }
    ],
    error: 0
  });
});

router.post("/rating/list", (req, res) => {
  res.send({
    count_all: 50,
    all: [
      {
        site: 1,
        name: "Мамкин арбитран",
        status:
          "Трафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайтТрафик лайт",
        modifier: "pro",
        like_count: 25252,
        change: 1,
        regalia: [],
        partner: "wecpa"
      },
      {
        site: 2,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: [
          "lead_machine",
          "beauty",
          "accessory_supplier",
          "health",
          "jobs",
          "fish",
          "tuning",
          "esoteric",
          "gamer",
          "professor"
        ],
        partner: "wecpa"
      },
      {
        site: 3,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: "pro",
        like_count: 25252,
        change: 0,
        regalia: ["accessory_supplier", "health"],
        partner: "wecpa"
      },
      {
        site: 4,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: "med",
        like_count: 25252,
        change: -11,
        regalia: ["jobs", "fish"],
        partner: "wecpa"
      },
      {
        site: 5,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["gamer", "professor"],
        partner: null
      },
      {
        site: 6,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["approve", "business_executive"],
        partner: "wecpa"
      },
      {
        site: 7,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["adult", "business_executive"],
        partner: null
      },
      {
        site: 8,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 9,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["adult", "business_executive"],
        partner: "wecpa"
      },
      {
        site: 10,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 11,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 12,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 13,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 14,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 15,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 16,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 17,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 18,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 19,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 20,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 21,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 22,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 23,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 24,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 25,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 26,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 27,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 28,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 29,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 30,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 31,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 32,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 33,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 34,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 35,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 36,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 37,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 38,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 39,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 40,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 41,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 42,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 43,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 44,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: null
      },
      {
        site: 45,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 46,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 47,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 48,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 49,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      },
      {
        site: 50,
        name: "Мамкин арбитран",
        status: "Трафик лайт",
        modifier: null,
        like_count: 25252,
        change: 1,
        regalia: ["tuning", "esoteric"],
        partner: "wecpa"
      }
    ],
    user: {
      site: 5,
      name: "Мамкин арбитран",
      status: "Трафик лайт",
      modifier: null,
      like_count: 25252,
      change: 1,
      regalia: ["gamer", "fashion"],
      partner: "wecpa"
    },
    error: 0
  });
});

router.get("/feed/options", (req, res) => {
  res.send({
    data: {
      new_offers: true,
      change_conditions: true,
      disabling_offers: true,
      payment_ordered: true,
      payment_made: true
    },
    error: 0
  });
});

router.post("/feed/options", (req, res) => {
  res.send({
    error: 0
  });
});

router.post("/feed/list", (req, res) => {
  res.send({
    count_all: 20,
    data: [
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-26T15:53:31+03:00",
        category: "Изменение условий по офферу",
        offer: "Автомагнитола Pioneer MVH-X580BT",
        comment:
          "RU: Изменились отчисления с 500 RUB на 600 RUB 2 Октября. Изменились отчисления с 500 RUB на 600 RUB 2 Октября. Изменились отчисления с 500 RUB на 600 RUB 2 Октября. Изменились отчисления с 500 RUB на 600 RUB 2 Октября.",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-26T15:52:31+03:00",
        category:
          "Новый оффер Новый оффер Новый оффер Новый оффер Новый оффер Новый оффер Новый оффер",
        offer: "Мишка PEEKABOO",
        comment: "RU: 1990₽/500₽",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-25T20:53:31+03:00",
        category: "Новый оффер",
        offer:
          "JBL Pulse 3 портативная акустическая система JBL Pulse 3 портативная акустическая система JBL Pulse 3 портативная акустическая система JBL Pulse 3 портативная акустическая система",
        comment: "RU: 1490₽/600 ₽",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-25T19:53:31+03:00",
        category: "Добавление лендинга",
        offer: "Мишка PEEKABOO",
        comment: "Лендинг 3",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-25T18:53:31+03:00",
        category: "Отключение оффера",
        offer: "Цифровой штангенциркуль",
        comment: "RU: 1990₽/500₽",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-24T20:53:31+03:00",
        category: "Изменений условий по офферу",
        offer: "Автомагнитола Pioneer MVH-X580BT",
        comment: "RU: Изменились отчисления с 500 RUB на 600 RUB",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-24T16:53:31+03:00",
        category: "Изменений условий по офферу",
        offer: "Автомагнитола Pioneer MVH-X580BT",
        comment: "RU: Изменились отчисления с 500 RUB на 600 RUB",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-24T15:53:31+03:00",
        category: "Изменений условий по офферу",
        offer: "Автомагнитола Pioneer MVH-X580BT",
        comment: "RU: Изменились отчисления с 500 RUB на 600 RUB",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T20:53:31+03:00",
        category: "Изменений условий по офферу",
        offer: "Автомагнитола Pioneer MVH-X580BT",
        comment: "RU: Изменились отчисления с 500 RUB на 600 RUB",
        offer_link: "https://www.google.ru/"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        category: "Изменений условий по офферу",
        offer: "Автомагнитола Pioneer MVH-X580BT",
        comment: "RU: Изменились отчисления с 500 RUB на 600 RUB",
        offer_link: "https://www.google.ru/"
      }
    ],
    error: 0
  });
});

router.get("/news/options", (req, res) => {
  res.send({
    data: {
      sources: {
        facebook: true,
        mt: true,
        google: true,
        yandex: true,
        teaser: true,
        push: true,
        vk: true
      },
      category: {
        cases: true,
        educational: true,
        interview: true,
        another_one: true,
        another_next: true
      },
      resources: {
        traffic_light: true,
        cpa_rip: true,
        partnerkin: true,
        zorbas_media: true
      }
    },
    error: 0
  });
});

router.post("/news/options", (req, res) => {
  res.send({
    error: 0
  });
});

router.post("/news/list", (req, res) => {
  res.send({
    count_all: 20,
    data: [
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description: "Вы можете настроить оповещения о запуске новых офферов"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "example",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступленииВы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      },
      {
        id: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
        time: "2019-04-23T16:53:31+03:00",
        img: "img/news-img.jpg",
        source: "Traffic Light",
        title: "Заголовок новости",
        description:
          "Вы можете настроить оповещения о запуске новых офферов, изменениях в условиях, отключения офферов, поступлении"
      }
    ],
    error: 0
  });
});

router.post("/news/get", (req, res) => {
  res.send({
    data: {
      date: "2019-04-23T16:53:31+03:00",
      next: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
      prev: "11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000",
      text:
        '<img src="img/news-item-img.jpg"></img>' +
        "<h1>Заголовок новости который может быть большим</h1>" +
        "<p>В товарных офферах холд перед первой выплатой  составляет 10 дней. Далее после проверки " +
        "траффика работаем без холда. В не товарных офферах сроки холда обсуждаются с рекламодателем " +
        "и могут быть разными. В товарных офферах холд перед первой выплатой  составляет 10 дней. Далее " +
        "после проверки траффика работаем без холда. В не товарных офферах сроки холда обсуждаются с" +
        " рекламодателем и могут быть разными. В не товарных офферах сроки холда обсуждаются с</p>" +
        "<p>после проверки траффика работаем без холда. В не товарных офферах сроки холда обсуждаются с" +
        " рекламодателем и могут быть разными. В не товарных офферах сроки холда обсуждаются с" +
        " рекламодателем и могут быть разными.</p>",
      link: "https://www.google.ru/",
      assessment: {
        like: 10000,
        dislike: 200000
      }
    }
  });
});

router.post("/news/vote", (req, res) => {
  res.send({
    error: 0
  });
});

router.get("/team/options", (req, res) => {
  res.send({
    statOptions: [
      {
        text: "Все видят статистику друг друга",
        value: 1
      },
      {
        text: "Члены команды видят статистику друг друга, но не видят статистику тимлида",
        value: 2
      },
      {
        text: "Только тимлид видит статистику всех членов команды",
        value: 3
      }
    ],
    payOptions: [
      {
        text: "Каждый член команды может выводить свой доход",
        value: 1
      },
      {
        text: "Только тимлид может выводить доход команды",
        popHint: {
          title: "Ограничения по вступлению и выходу из команды",
          body: `Для вступления вебмастера в вашу команду и последующего выхода из команды необходимо соблюдение условий:<br>1. на балансе вебмастера не должно быть средств;<br>2. у вебмастера не должно быть лидов в обработке`
        },
        bottomHint:
          "Вебмастера смогут выводить доход только на Eprofit или ZaleyCash, а также совершать покупки в Лайт store за накопленные лайты. Остальные методы остальные методы вывода доступны только тимлиду",
        value: 2
      }
    ],
    deleteFromTeamEvents: [
      {
        text: "Тимлид должен вывести ваши средства на свой баланс",
        success: true
      },
      { text: "Обработаются все ваши лиды", success: false }
    ],
    conditionOptionResult: [
      {
        text: "Вы не можете присоединиться к команде пока на вашем балансе есть средства",
        success: true
      },
      {
        text:
          "Вы не сможете выйти из команды до тех пор, пока у вас есть средства на балансе и лиды в обработке",
        success: true
      },
      {
        text:
          "Вы не сможете выводить деньги никуда кроме Eprofit и ZaleyCash (все платежные методы будут доступны только тимлиду)",
        success: true
      }
    ]
  });
});

router.get("/teams", (req, res) => {
  let teams = [
    {
      id: "sdfssdf8s89df798sd7sd",
      link: "",
      linkExpired: "24 часа 23 минуты",
      status: "",
      name: "Новая команда",
      img: "img/team-blank.jpg",
      am_i_teamlid: true,
      options: {
        statOption: 1,
        payOption: 1
      }
    },
    {
      id: "sdfssdf8s89df798sd7sd3",
      link: "",
      linkExpired: "3 часа 3  минуты",
      status: "nobody can see stat",
      name: "Nobody see!",
      img: "img/team-blank.jpg",
      am_i_teamlid: false,
      options: {
        statOption: 3,
        payOption: 1
      }
    },
    {
      id: "sdfssdf8s89df798sd7sd2",
      link: "",
      linkExpired: "4 часа 4  минуты",
      status: "can see masters stat, not teamlid!",
      name: "All see, but not teamlid!",
      img: "img/team-blank.jpg",
      am_i_teamlid: false,
      options: {
        statOption: 2,
        payOption: 1
      }
    }
  ];

  if (req.query.limit) {
    teams = teams.splice(0, req.query.limit);
  }

  res.send(teams);
});

const masters = [
  {
    id: "873948573jsdljfl",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd7",
    is_lead: true,
    name: "IamTeamLid",
    confirm_lids: 0,
    tested_offers: 0,
    summ_work: 0,
    balance_rub: 0,
    balance_usd: 0,
    balance_eur: 0,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdljfl2",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd",
    is_lead: true,
    name: "LoginTeamLid",
    confirm_lids: 1,
    tested_offers: 1,
    summ_work: 50,
    balance_rub: 1000,
    balance_usd: 16,
    balance_eur: 12,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdljfl3",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd",
    is_lead: false,
    name: "webmaster1",
    confirm_lids: 0,
    tested_offers: 0,
    summ_work: 0,
    balance_rub: 0,
    balance_usd: 0,
    balance_eur: 0,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdljfl4",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd",
    is_lead: false,
    name: "webmaster2",
    confirm_lids: 0,
    tested_offers: 0,
    summ_work: 0,
    balance_rub: 0,
    balance_usd: 0,
    balance_eur: 0,
    change: 1,
    status: 1,
    active: false
  },
  {
    id: "873948573jsdljfl6",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd",
    is_lead: false,
    name: "webmaster3",
    confirm_lids: 0,
    tested_offers: 0,
    summ_work: 0,
    balance_rub: 0,
    balance_usd: 0,
    balance_eur: 0,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdljfl5",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd",
    is_lead: false,
    name: "webmaster4",
    confirm_lids: 0,
    tested_offers: 0,
    summ_work: 0,
    balance_rub: 0,
    balance_usd: 0,
    balance_eur: 0,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdljfl2",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd2",
    is_lead: false,
    name: "LoginTeamLid 8",
    confirm_lids: 1000,
    tested_offers: 1000,
    summ_work: 10,
    balance_rub: 100,
    balance_usd: 100,
    balance_eur: 100,
    change: 1,
    status: 1,
    active: false
  },
  {
    id: "873948573jsdljfl2",
    modifier: "pro",
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd2",
    is_lead: true,
    name: "Team lid!",
    confirm_lids: 1000,
    tested_offers: 1000,
    summ_work: 10,
    balance_rub: 100,
    balance_usd: 100,
    balance_eur: 100,
    change: 1,
    status: 1,
    active: false
  },
  {
    id: "873948573jsdljflsdf",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd2",
    is_lead: false,
    name: "LoginTfeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdljflxcv",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd2",
    is_lead: true,
    name: "TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdljfadsfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "87394sdfv8573jsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "87sdf3948573jsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jsdlsdfsjfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: false
  },
  {
    id: "234873948573jsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "87sdg23948573jsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: true,
    name: "TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "werv873948573jsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "8739sdfyhg48573jsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "87fwf232233948573jsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  },
  {
    id: "873948573jfffsdljfl",
    modifier: null,
    img: "img/master-blank.jpg",
    team_id: "sdfssdf8s89df798sd7sd3",
    is_lead: false,
    name: "Login2TeamLid",
    confirm_lids: 50,
    tested_offers: 23,
    summ_work: 10,
    balance_rub: 1000,
    balance_usd: 1000,
    balance_eur: 1000,
    change: 1,
    status: 1,
    active: true
  }
];

router.post("/web-masters/list", (req, res) => {
  res.send(masters);
});

router.post("/web-masters/list/:teamId", (req, res) => {
  res.send(masters.filter(m => m.team_id === req.params.teamId));
});

module.exports = router;

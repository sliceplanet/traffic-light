# traffic_light

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### API

#### User

```javascript
GET /api/user/get

Permission: Login

Success 200

response: {
    auth: Boolean,
    purse: {
        currency: Int,
        bonus: Int
    },
    user: String // как на счет UIID_v4 ? или e-mail
}

error

response: {
    error: Int,
    error_text: String
}
```

#### Notifications

```javascript
GET /api/notifications/list

Permission: Login

Success 200

response: {
    count_all: Int,
    data: [{
        object: String // "Dashbord",
        count: Int
    }],
    error: Int // если все ок - 0
}

error

response: {
    error: Int,
    error_text: String
}
```

#### Achievements

```javascript
POST /api/achievements/list

Permission: Login

Success 200

req: {
    user: String
};

res: {
    count_all: Int,
    data: [{
        title: String, // Протестировать 3 офера
        reward: Int,
        stage_current: Int,
        stage_all: Int
    }],
    error: Int
}

error

response: {
    error: Int,
    error_text: String
}
```


#### Operating

```javascript
POST /api/operating/list

Permission: Login

Success 200

req: {
    user: String,
    period: String // today, at_week, at_month; PS здесь вопрос, т.к. таким способом привяжем время к времени сервера, а не клиента
    raw_leads: Boolean,
    previous: Boolean
};

res: {
    count_all: Int,
    leads:{
        count: Int,
        state: String // up, down, without_change
    },
    ranking: {
        site: Int,
        state: String // up, down, without_change
    },   
    income: {
        currency: Int,
        bonus: Int
    },
    data: {
        current: [{ // Arr of obj
            value: Int,
            time: String // Date format ISO8601
        }],
        old: [{ // Arr of obj или null если req.previous == false
           value: Int,
           time: String // Date format ISO8601
        }] 
    }
    error: Int
}

error

response: {
    error: Int,
    error_text: String
}
```


#### Offers

```javascript
POST /api/offers/list

Permission: Login

Success 200

req: {
   user: String
}

response: {
    count_all: Int,
    data: [{
        img: String, // path to file
        title: String,
        time: String // Date format ISO8601,
        price: [
            country: String,
            value: Int //вопрос, что означает второе число (400р)? 1290₽/400₽
        ]
    }],
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```


#### Rating

```javascript
POST /api/rating/list

Permission: Login

Success 200

req: {
    period: String // today, at_week, at_month;
}

response: {
    count_all: Int,
    data: [{
        site: Int,
        name: String,
        status: String,
        like_count: Int,
        change: {
            value: Int,
            stage: String // up, down, without_change
        },
        regalia: [
            'beauty', // подготовьте список всех регалий 
            'provider'
        ] 
    }],
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```


#### User rating

```javascript
POST /api/rating/get

Permission: Login

Success 200

req: {
     user: String
}

response: {
    data: {
        site: Int,
        name: String,
        status: String,
        like_count: Int,
        change: {
            value: Int,
            stage: String // up, down, without_change
        },
        regalia: [
            'beauty', // подготовьте список всех регалий 
            'provider'
        ] 
    },
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### Regalia list

```javascript
POST /api/regalia/list

Permission: Login

Success 200

req: {
     user: String
}

response: {
    data: [{ // список всех регалий
        title: String,
        value: Int // от 0 до 5, если больше 0 - значит пользователь имеет прогресс по этому достижению
    }],
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### Feed options

```javascript
POST /api/feed/options

Permission: Login

Success 200

req: {
     user: String
}

response: {
    data: [{ // список настроек отображения рейтинга, нужен список вообще всего, т.к. многого нет на макете
        section: String, // traffic - источники трафика
        options: [{
            facebook: Boolean,
            MIT: Boolean
        }]
    }],
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### Feed options change
Отправить запрос на изменение фильтров
```javascript
POST /api/feed/options/change

Permission: Login

Success 200

req: {
    user: String,
    data: [{ 
        section: String, // traffic - источники трафика
        options: [{
            facebook: Boolean,
            MIT: Boolean
        }]
    }]
}

response: {
    status: Sting // ok
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### Feed list

```javascript
POST /api/feed/list

Permission: Login

Success 200

req: {
     user: String,
     options: [{
         facebook: Boolean,
         MIT: Boolean
     }]
}

response: {
    count_all: Int,
    data: [{
        id: String // uuidv4
        time: String, // Date format ISO8601,
        category: Sting,
        offer: Sting,
        comment: Sting
    }],
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### Feed item

На макете нет информации о данных одной единицы в ленте, разъяснить

```javascript
POST /api/feed/get

Success 200

req: {
    id: String
}

response: {
    title: Sting,
    description: String,
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### News options
получить список используемых фильтров
```javascript
POST /api/news/options

Permission: Login

Success 200

req: {
     user: String
}

response: {
    data: [{ 
        section: String, // traffic - источники трафика
        options: [{
            facebook: Boolean,
            MIT: Boolean
        }]
    }],
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### News options change
Отправить запрос на изменение фильтров
```javascript
POST /api/news/options/change

Permission: Login

Success 200

req: {
    user: String,
    data: [{ 
        section: String, // traffic - источники трафика
        options: [{
            facebook: Boolean,
            MIT: Boolean
        }]
    }]
}

response: {
    status: Sting // ok
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```


#### News

```javascript
POST /api/news/list

Success 200

req: {
    user: String,
    options: [{
        facebook: Boolean,
        MIT: Boolean
    }]
}

response: {
    count_all: Int,
    data: [{
            id: String // uuidv4
            time: String, // Date format ISO8601,
            title: String,
            desctiption: Sting, // max length - 100
    }],
    error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### News Item
что значит открыть полностью?

```javascript
POST /api/news/get

Success 200

req: {
    id: String // uiidv4  
}

response: {
   title: String,
   desctiption: Sting,
   assessment: {
   like: Int,
   dislake: Int,
  }
   error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

#### News assessment

```javascript
POST /api/assessment/news

Success 200

req: {
   user: Sting,
   assessment: 'like' // like or dislake
}

response: {
   status: Sting,
   error: Int 
}

error

response: {
    error: Int,
    error_text: String
}
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

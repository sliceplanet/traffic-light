const express = require('express');
const path = require('path');
const logger = require('morgan');


const app = express();

app.use(logger('common'));

app.use(express.json());
app.use(express.urlencoded({extended: false}));

require('../server/api/configure')(app);

app.use(express.static(path.join(__dirname, '../client/dist')));

console.log('init');

module.exports = app;
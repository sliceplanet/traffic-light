import Vue from "vue";
import Router from "vue-router";

import Dashboard from "./components/Dashboard";
import ArbitrationTeam from "./components/ArbitrationTeam";
import MyTeam from "./components/MyTeam";
import MyTeamOne from "./components/MyTeamOne";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "dashboard",
      component: Dashboard
    },
    {
      path: "/arbitration-team",
      name: "arbitration-team",
      component: ArbitrationTeam
    },
    {
      path: "/invite/:code",
      name: "invite-to-team",
      component: ArbitrationTeam
    },
    {
      path: "/my-team",
      name: "my-team",
      component: MyTeam
    },
    {
      path: "/my-team-one-example",
      name: "my-team-one-example",
      component: MyTeamOne
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});

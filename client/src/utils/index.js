import moment from "moment";

export function createTime(period) {
    let day = moment().format();
    let week = moment().subtract(7,'d').format();
    let month = moment().subtract(1, 'months').format();
    if (period == 'today') {
        return [day, day];
    } else if (period == 'at_week') {
        return [week, day];
    } else if (period == 'at_month') {
        return [month, day];
    }
}

export function formatPrice(price) {
    return String(price).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, "$1 ");
}

export function addPlus(value) {
    if (value > 0) {
        return '+' + value;
    } else {
        return value
    }
}

export function sortTime(target, asc) { // true - asc, false - desc
    target.sort((a, b) => Date.parse(a.time) - Date.parse(b.time) );

    return asc ? target : target.reverse();
}

export function parseTime(time) { // заменяет строку time в объекте на объект с полями: месяц. год, день, час, минуты
    let date = moment(time);
    return {
        year: date.year(),
        month: date.month(),
        day: date.date(),
        hour: date.hour(),
        minute: date.minute()
    };
}

export function dailyGrouping(array) {
    let items = array.slice();
    let result = [];
    let checked = [];

    for (let i = 0; i < items.length; i++) {
        let day = items[i].time.day;
        let month = items[i].time.month;
        let year = items[i].time.year;
        let group = items.filter((item, index) => {
            if(item.time.day == day && item.time.month == month && item.time.year == year && !checked.includes(index)) {
                checked.push(index);
                return item;
            } else {
                return false
            }
        });
        if (group.length) {
            result.push({
                time: {
                    day: day,
                    month: month,
                    year: year
                },
                group: group
            });
        }
    }

    return result;
}

export function formatTime(time) {
    return moment(time).format("DD.MM.YYYY")
}

export function addEvent(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on"+type] = callback;
    }
}

export function windowResize(callback) {
    addEvent(window, "resize", callback);
}
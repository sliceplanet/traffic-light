const configureAPI = require('../server/api/configure');

module.exports = {
    css: {
        sourceMap: true
    },
    devServer: {
        before: configureAPI
    }
};

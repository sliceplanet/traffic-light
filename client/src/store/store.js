import Vue from "vue";
import Vuex from "vuex";
import createPersistedState from "vuex-persistedstate";
import team from "./modules/team";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    team: team
  },
  // plugins: [createPersistedState()],
  state: {
    sidebar_open: window.innerWidth > 1024,
    feed: false,
    webMaster: false,
    rating: false,
    news: false,
    os_old_period: true
  },
  mutations: {
    SIDEBAR_OPEN(state, sidebar_open) {
      state.sidebar_open = sidebar_open;
    },
    FEED_OPEN(state, feed) {
      state.feed = feed;
    },
    RATING_OPEN(state, rating) {
      state.rating = rating;
    },
    WEB_MASTER_OPEN(state, master) {
      state.webMaster = master;
    },
    NEWS_OPEN(state, news) {
      state.news = news;
    },
    OS_OLD_PERIOD_CHANGE(state, value) {
      state.os_old_period = value;
    }
  },
  actions: {
    SidebarOpen: ({ commit }, data) => {
      commit("SIDEBAR_OPEN", data);
    },
    FeedOpen: ({ commit }, data) => {
      commit("FEED_OPEN", data);
    },
    RatingOpen: ({ commit }, data) => {
      commit("RATING_OPEN", data);
    },
    WebMasterOpen: ({ commit }, data) => {
      commit("WEB_MASTER_OPEN", data);
    },
    NewsOpen: ({ commit }, data) => {
      commit("NEWS_OPEN", data);
    },
    OsOldPeriodChange: ({ commit }, data) => {
      commit("OS_OLD_PERIOD_CHANGE", data);
    }
  },
  getters: {
    sidebar_open: state => state.sidebar_open,
    feed: state => state.feed,
    rating: state => state.rating,
    webMaster: state => state.webMaster,
    news: state => state.news,
    os_old_period: state => state.os_old_period
  }
});
